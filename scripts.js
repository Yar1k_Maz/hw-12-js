const buttons = document.querySelectorAll(".btn");
const buttonState = {};
function updateButtonState(key) {
  buttons.forEach((button) => {
    button.classList.remove("active");
  });
  if (buttonState[key]) {
    buttonState[key].classList.add("active");
  }
}
document.addEventListener("keydown", (event) => {
  const key = event.key.toUpperCase();
  if (buttonState[key]) {
    buttonState[key].classList.remove("active");
  }
  buttonState[key] = document.querySelector(`.btn:contains('${key}')`);
  updateButtonState(key);
});

buttons.forEach((button) => {
  button.addEventListener("blur", (event) => {
    const key = event.target.textContent;
    buttonState[key] = null;
    updateButtonState(key);
  });
});